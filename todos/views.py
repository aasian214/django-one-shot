from django.shortcuts import render, get_object_or_404
from todos.models import TodoItem, TodoList

# Create your views here.
def todolist(request):
    todos = TodoList.objects.all()
    context = {
        "todolist": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo_object": todo,
    }
    return render(request, "todos/details.html", context)
