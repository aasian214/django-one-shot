from django.urls import path
from todos.views import todolist, show_todo

urlpatterns = [
    path("", todolist, name="todo_list_list"),
    path("<int:id>", show_todo, name="show_todo"),
]
